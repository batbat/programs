/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 8;       /* snap pixel */
static const int swallowfloating    = 0;	/* 1 means swallow floating windows by default */
static const unsigned int gappih    = 8;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 8;       /* vert inner gap between windows */
static const unsigned int gappoh    = 8;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 8;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Ubuntu-R:size=12", "Twitter Color Emoji:pixelsize=14" };
static const char dmenufont[]       = "Ubuntu-R:size=10";
static const char col_gray1[]       = "#3b4252";
static const char col_gray2[]       = "#5e81ac";
static const char col_gray3[]       = "#d8dee9";
static const char col_gray4[]       = "#eceff4";
static const char col_cyan[]        = "#a3be8c";
static const char col_black[]       = "#2e3440";
static const char col_lgray[]       = "#4c566a";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray2,  col_cyan  },
	[SchemeStatus]  = { col_gray3, col_black,  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_gray4, col_lgray,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
	[SchemeTagsNorm]  = { col_gray3, col_black,  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel]  = { col_gray3, col_black,  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm]  = { col_gray3, col_black,  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "st-256color", NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "Tiling",      tile },    /* first entry is default */
	{ "Floating",      NULL },    /* no layout function means floating behavior */
	{ "Monocle",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,			XK_equal,  incrgaps,       {.i = +4 } },
	{ MODKEY,			XK_minus,  incrgaps,       {.i = -3 } },
	{ MODKEY|ShiftMask,		XK_minus,  togglegaps,     {0} },
	{ MODKEY|ShiftMask,		XK_equal,  defaultgaps,    {0} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,			XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_space,  setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,		XK_t,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
/* Keybinds */
	{ MODKEY|ShiftMask,		XK_b,	   spawn,	   SHCMD("brave") },
	{ MODKEY|ShiftMask,		XK_z,	   spawn,	   SHCMD("st -e ncmpcpp") },
	{ MODKEY|ShiftMask,		XK_a,	   spawn,	   SHCMD("st -e pulsemixer") },
	{ MODKEY,			XK_w,	   spawn,	   SHCMD("feh --bg-fill --randomize -r ~/wallpapers/*") },
	{ MODKEY,			XK_y,	   spawn,	   SHCMD("ytfzf -D") },
	{ MODKEY,			XK_n,	   spawn,	   SHCMD("st -e newsboat") },
/* Volume Controls */
	{ 0,				0x1008FF12,spawn,	   SHCMD("pamixer -t; kill -36 $(pidor dwmblocks)") },
	{ 0,				0x1008FF13,spawn,	   SHCMD("pamixer --allow-boost -i 5; kill -36 $(pidof dwmblocks)") },
	{ 0,				0x1008FF11,spawn,	   SHCMD("pamixer --allow-boost -d 5; kill -36 $(pidof dwmblocks)") },
	{ 0,				XK_F6,	   spawn,	   SHCMD("pamixer -t; kill -36 $(pidof dwmblocks)") },
	{ 0,				XK_F8,	   spawn,	   SHCMD("pamixer --allow-boost -i 5; kill -36 $(pidof dwmblocks)") },
	{ 0,				XK_F7,	   spawn,	   SHCMD("pamixer --allow-boost -d 5; kill -36 $(pidof dwmblocks)") },
/* Audio Controls */
	{ 0,				0x1008FF14,spawn,	   SHCMD("mpc stop") },
	{ 0,				0x1008FF16,spawn,	   SHCMD("mpc prev") },
	{ 0,				0x1008FF17,spawn,	   SHCMD("mpc next") },
	{ 0,				XK_F10,    spawn,	   SHCMD("mpc toggle") },
	{ 0,				XK_F9,	   spawn,	   SHCMD("mpc seek -10") },
	{ 0,				XK_F11,    spawn,	   SHCMD("mpc seek +10") },
/* Brightness Controls */
	{ 0,				0x1008FF02,spawn,	   SHCMD("sudo xbacklight -inc 5; kill -37 $(pidof dwmblocks)") },
	{ 0,				0x1008FF03,spawn,	   SHCMD("sudo xbacklight -dec 5; kill -37 $(pidof dwmblocks)") },
	{ 0,            		XK_F3,	   spawn,          SHCMD("sudo xbacklight -inc 5; kill -37 $(pidof dwmblocks)") },
        { 0,            		XK_F2,	   spawn,          SHCMD("sudo xbacklight -dec 5; kill -37 $(pidof dwmblocks)") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

