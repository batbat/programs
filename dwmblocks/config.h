//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/				/*Update Interval*/	/*Update Signal*/
	{"",		"~/.local/bin/statusbar/sb-music",	0,			7},
	{"",		"~/.local/bin/statusbar/sb-internet",	2,			5},
	{"",		"~/.local/bin/statusbar/sb-volume",	10,			2},
	{"",		"~/.local/bin/statusbar/sb-brightness",	0,			3},
	{"",		"~/.local/bin/statusbar/sb-battery",	5,			4},
	{"",		"~/.local/bin/statusbar/sb-weather",	3600,			1},
	{"",		"~/.local/bin/statusbar/sb-clock",	1,			6},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
